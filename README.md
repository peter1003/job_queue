# How To Use It #

# System Requirements

# docker

## Enter Docker Container

	docker-compose run app bash

## Run Consumer

	cd sample/consumer
	go run consumer.go
## Run Publisher
	
	cd sample/publisher
	go run publisher.go

