package src

import (
	"fmt"
	"os"
)

var (
	allQueueEngine map[string]QueueEngine
	currentConfig  *Configuration
)

func SetConfig(config *Configuration) {
	currentConfig = config
}

func getConfig() *Configuration {
	if currentConfig == nil {
		fmt.Println("NIL")
		//這裡可以改成自己的設置
		currentConfig = &Configuration{
			Engine:        os.Getenv("QUEUE_ENGINE"),
			ConnectionURL: os.Getenv("QUEUE_CONNECT_URL"),
		}
	}
	return currentConfig
}
func StartQueue(queues ...string) {
	switch engine := getConfig().Engine; engine {
	case "rabbitMQ":
	case "rabbitmq":
		allQueueEngine["rabbitMQ"].Connect(getConfig().ConnectionURL, queues...)
	default:
		fmt.Println("Not found Queue Engine")
	}
}

func CurrentQueue() QueueEngine {
	switch engine := getConfig().Engine; engine {
	case "rabbitMQ":
	case "rabbitmq":
		return allQueueEngine["rabbitMQ"]
	default:
		fmt.Println("Not found Queue Engine")
	}
	return nil
}

/**
 * 增加支援的Engine
 * @param {[type]} engineName string      [description]
 * @param {[type]} engine     QueueEngine [description]
 */
func addEngine(engineName string, engine QueueEngine) {
	allQueueEngine[engineName] = engine
}

func init() {
	allQueueEngine = map[string]QueueEngine{}
}
