package src

import (
	"errors"
	"fmt"
	"os"
)

var (
	publishConfig *Configuration
)

func SetPublishConfig(config *Configuration) {
	publishConfig = config
}

func getPublishConfig() *Configuration {
	if publishConfig == nil {
		fmt.Println("publishConfig Nil")
		//這裡可以改成自己的設置
		publishConfig = &Configuration{
			Engine:        os.Getenv("QUEUE_ENGINE"),
			ConnectionURL: os.Getenv("QUEUE_CONNECT_URL"),
		}
	}
	return publishConfig
}

func StartQueueOnPublish() (err error) {
	switch engine := getPublishConfig().Engine; engine {
	case "rabbitMQ":
	case "rabbitmq":
		err = allQueueEngine["rabbitMQ"].ConnectOnPublisher(getPublishConfig().ConnectionURL)
	default:
		fmt.Println("Not found Queue Engine")
		err = errors.New("Not found Queue Engine")
	}
	return err

}
func AddTask(queueName string, addTask NewTask) error {
	err := StartQueueOnPublish()
	defer CurrentQueue().Close()
	err = CurrentQueue().AddTask(queueName, addTask)
	return err
}
