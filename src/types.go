package src

import (
	"time"
)

type NewTask struct {
	TaskName string
	Content  string
	Delay    time.Duration
}

type QueueEngine interface {
	Connect(string, ...string) error
	ConnectOnPublisher(string) error
	Close()
	AddTask(string, NewTask) error
}
