package src

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"log"
	"os"
	"time"
)

type RabbitMQ struct {
	engine QueueEngine
}

var (
	connection  *amqp.Connection
	amqpChannel *amqp.Channel
)

func (q *RabbitMQ) Connect(queueUrl string, queues ...string) error {

	q.commonConnect(queueUrl)
	if len(queues) == 0 {
		//建立預設的Queue
		queue := makeQueue("slot")
		makeChannel(queue)
	}
	return nil
}

func (q *RabbitMQ) ConnectOnPublisher(queueUrl string) error {
	err := q.commonConnect(queueUrl)
	return err

}
func (q *RabbitMQ) commonConnect(queueUrl string) error {
	url := queueUrl
	//If it doesn't exist, use the default connection string.
	if url == "" {
		//Don't do this in production, this is for testing purposes only.
		url = "amqp://guest:guest@queue:5672"
	}

	// Connect to the rabbitMQ instance
	connection, err := amqp.Dial(url)
	handleError(err, "Can't connect to AMQP")
	amqpChannel, err = connection.Channel()
	handleError(err, "Can't create a amqpChannel")
	return err
}
func makeQueue(queueName string) *amqp.Queue {
	queue, err := amqpChannel.QueueDeclare(queueName, true, false, false, false, nil)
	handleError(err, "Could not declare `add` queue")

	err = amqpChannel.Qos(1, 0, false)
	handleError(err, "Could not configure QoS")
	return &queue
}

func makeChannel(queue *amqp.Queue) {
	messageChannel, err := amqpChannel.Consume(
		queue.Name,
		"",
		false,
		false,
		false,
		false,
		nil,
	)
	handleError(err, "Could not register consumer")

	stopChan := make(chan bool)

	go func() {
		log.Printf("Consumer ready, PID: %d", os.Getpid())
		for d := range messageChannel {
			log.Printf("Received a message: %s", d.Body)

			addTask := &NewTask{}

			err := json.Unmarshal(d.Body, addTask)

			if err != nil {
				log.Printf("Error decoding JSON: %s", err)
			}

			log.Printf("Sleep %d seconds", addTask.Delay)
			time.Sleep(addTask.Delay * time.Second)

			log.Printf("Result of %s + %s ", addTask.TaskName, addTask.Content)

			if err := d.Ack(false); err != nil {
				log.Printf("Error acknowledging message : %s", err)
			} else {
				log.Printf("Acknowledged message")
			}

		}
	}()

	// Stop for program termination
	<-stopChan
}

func (q *RabbitMQ) Close() {
	if connection != nil {
		connection.Close()
	}
	if amqpChannel != nil {
		amqpChannel.Close()
	}
}

func (q *RabbitMQ) AddTask(queueName string, addTask NewTask) error {
	queue := makeQueue(queueName)
	body, err := json.Marshal(addTask)
	if err != nil {
		handleError(err, "Error encoding JSON")
	}

	err = amqpChannel.Publish("", queue.Name, false, false, amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "text/plain",
		Body:         body,
	})

	if err != nil {
		log.Fatalf("Error publishing message: %s", err)
	}

	log.Printf("AddTask: %s+%s, delay : %d", addTask.TaskName, addTask.Content, addTask.Delay)
	return err
}

func init() {
	e := new(RabbitMQ)
	addEngine("rabbitMQ", e)
}
