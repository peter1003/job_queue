package src

type Configuration struct {
	Engine        string
	ConnectionURL string
}
