module bitbucket.org/peter1003/queue

go 1.15

require (
	bitbucket.org/peter1003/common v0.0.0-20200910074614-20da90747c83
	github.com/streadway/amqp v1.0.0
)
