package main

import (
	queue "bitbucket.org/peter1003/queue/src"
	"os"
)

func main() {
	config := &queue.Configuration{
		Engine:        os.Getenv("QUEUE_ENGINE"),
		ConnectionURL: os.Getenv("QUEUE_CONNECT_URL"),
	}
	queue.SetPublishConfig(config)
	queue.AddTask("slot", queue.NewTask{"One", "1", 0})
	queue.AddTask("slot", queue.NewTask{"Two", "2", 10})
}
