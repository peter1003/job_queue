package main

import (
	queue "bitbucket.org/peter1003/queue/src"
	"os"
)

func main() {
	config := &queue.Configuration{
		Engine:        os.Getenv("QUEUE_ENGINE"),
		ConnectionURL: os.Getenv("QUEUE_CONNECT_URL"),
	}
	queue.SetConfig(config)
	queue.StartQueue()
	defer queue.CurrentQueue().Close()
}
